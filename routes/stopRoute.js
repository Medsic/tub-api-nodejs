var stopController = require('../app/controllers/stopController');
 
module.exports = {
	configure: function(app) {
		app.get('/stop/getAll', function(req, res) {
			stopController.getAll(req, res);
		});

		app.get('/stop/getAllStops', function(req, res) {
			stopController.getAllStops(req, res);
		});

		app.get('/stop/getById', function(req, res) {
		 	stopController.getById(req, res);
		});

		app.get('/stop/getStopsBetween', function(req, res) {
		 	stopController.getStopsBetween(req, res);
		});

		app.get('/stop/getETA', function(req, res) {
			stopController.getETA(req, res);
		});

		app.post('/stop/addStop', function(req, res) {
			console.log(req);
			stopController.addStop(req, res);
		});
	}
};