var mysql = require('mysql');
 
var connection = mysql.createConnection({
  host: 'localhost',
  port: '3306',
  user: 'tub',
  password: 'tub',
  database: 'tub'
});

connection.connect(function(err) {
  console.log("connection to db");
  if (err) {
    console.error('Error Connecting: ' + err.stack);
    return;
  }
});
 
module.exports = connection;