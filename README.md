Tub API Rest Node.js
---
---

**Base API URL :** http://localhost:8000/

Cloning the API
---
- **Clone the repo :**
Get into the directory of your choice and execute this command : `git clone https://aturlier@bitbucket.org/Medsic/tub-api-nodejs.git && cd tub-api-nodejs`

Starting the API
---
- **Start the servers :**
Launch your servers using MAMP, or any other similar tool. We defined the connection properties in the `connection.js` file. Feel free to change the properties according to your configuration.

- **Populate the SQL database :**
Open the `tub_api_script.sql` with your favorite tool. We recommend Sequel Pro (MacOS)

- **Start the api :**
Go into the repository where you cloned this repo, at the `app.js` level. Then execute this command : `node app.js`. At this point, the API should wait for your requests.

- **Request data to the API :**
Send a request to ou API using one of the calls below. You can use Postman or built your own app that consumes our API.

Calls
---

- **Get all stops :** stop/getAll

        return a list of stops

- **Get all stops but distinct** stop/getAllStops

        return a distinct list of stops

- **Get a stop by ID :** stop/getById?id=#
 
        return a stop

- **Get arrival date time for an arrival stop :** stop/getETA?firstOrder=#&firstSchedule=#&lastOrder=#

        query : firstOrder (departure stop order), firstSchedule (departure schedule), lastOrder (arrival stop order)
        return a stop

- **Post a new stop :** stop/addStop
    
        body : stopName, stopOrder, stopNextSchedule
        
JSon Stop Object :

        {
            "id": 1,
            "name": "Molière",
            "order": 1,
            "nextSchedule": 12:30:00
        }
