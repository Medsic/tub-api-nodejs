CREATE DATABASE tub;
USE tub;
CREATE USER 'tub'@'localhost' IDENTIFIED BY 'tub';
GRANT ALL ON stops TO 'tub'@'localhost';
CREATE TABLE stops (
    `id` int NOT NULL AUTO_INCREMENT,
    `name` varchar(255),
    `order` varchar(255),
    `nextSchedule` time,
    PRIMARY KEY (`id`)
);

INSERT INTO stops VALUES (NULL, "Molière", "1", "12:00:00");
INSERT INTO stops VALUES (NULL, "Molière", "0", "11:00:00");
INSERT INTO stops VALUES (NULL, "Verlaine", "1", "12:10:00");
INSERT INTO stops VALUES (NULL, "Verlaine", "0", "10:50:00");