var stopModel = require('../models/stopModel');

var getAll = function(req, res) {
    stopModel.getAll(function(err, rows) {
        if (err) {
            res.send(err);
        }
        res.json(rows);
    });
}

var getAllStops = function(req, res) {
    stopModel.getAllStops(function(err, rows) {
        if (err) {
            res.send(err);
        }
        res.json(rows);
    });
}

var getById = function(req, res) {
    stopModel.getById(req.query.id, function(err, rows) {
        if (err) {
            res.send(err);
        }
        res.json(rows);
    });
}

var getStopsBetween = function(req, res) {
    stopModel.getStopsBetween(req.query, function(err, rows) {
        if (err) {
            res.send(err);
        }
        res.json(rows);
    });
}

var getETA = function(req, res) {
    stopModel.getETA(req.query, function(err, rows) {
        if (err) {
            res.send(err);
        }
        res.json(rows);
    })
}

var addStop = function(req, res) {
    stopModel.addStop(req.body, function(err, rows) {
        if(err){
            res.send(err);
        }
        res.json(rows);
    });
}
module.exports = {
    getAll:getAll,
    getAllStops:getAllStops,
    getById:getById,
    getStopsBetween:getStopsBetween,
    getETA:getETA,
    addStop:addStop
}