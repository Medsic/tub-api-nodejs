var connection = require('../../connection');
 
function Stop() {
	this.getAll = function(req, callback) {
		connection.query('select * from stops', function(err, result) {
			req(result);
		});
	};

	this.getAllStops = function(req, callback) {
		connection.query('select * from stops group by name', function(err, result) {
			req(result);
		});
	};

	this.getById = function(id, req, callback) {
		connection.query('select * from stops where id = ?', id, function(err, result) {
			req(result);
		});
	};

	this.getStopsBetween = function(query, req, callback) {
		var sqlQuery = "select * from stops where `order` between " +
			"(select `order` from stops where `order` = `" + query.first + "` limit 1) AND " +
			"(select `order` from stops where `order` = `" + query.last + "` limit 1) GROUP BY name ORDER BY `order`";
		connection.query(sqlQuery,
			function(err, result) {
			req(result);
		});
	};

	this.getETA = function(query, req, callback) {
		var sqlQuery = "SELECT * FROM stops " + 
    			"WHERE nextSchedule = (SELECT MIN(nextSchedule) " +
				"FROM stops " +
				"WHERE time(nextSchedule) > time(" + query.firstSchedule + ") " +
				"AND `order` = " + query.lastOrder + " " +
				"AND `order` NOT LIKE " + query.firstOrder + ")";
    			console.log(sqlQuery);
		connection.query(sqlQuery, function(err, result) {
				req(result);
		});
	}

	this.addStop = function(body, req, res) {
		console.log(body);
		connection.query('insert into stops values (null, \'' 
								+ body.stopName + '\', ' 
								+ body.stopOrder +  '\', '
								+ body.stopNextSchedule + ')',
								function(err, result){
									req(result);
								});
	}
};

module.exports = new Stop();